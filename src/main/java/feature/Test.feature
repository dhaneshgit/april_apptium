Feature: Bointo login validation

@Passive
Scenario Outline: Positive Data
Given Open the browser
And Enter the URL
And max the browser
When Enter the username as <uname>
When Enter the password as <pass>
And click login button 
And click TELUS 
Then Verify the Login page
And close the browser

Examples:
|uname||pass|
|dsridharan@apptium.com||TheGame@123|
#|123@apptium.com||Pass@123|

@~neg
Scenario Outline: Negative Data
Given Open the browser
And Enter the URL
And max the browser
When Enter the username as <uname>
When Enter the password as <pass>
And click login button 
And click TELUS 
Then Verify the Login page
And close the browser

Examples:
|uname||pass|
|123@apptium.com||Pass@123|

@neg
Scenario Outline: Negative Data
Given Open the browser
And Enter the URL
And max the browser
When Enter the username as <uname>
When Enter the password as <pass>
And click login button 
And click TELUS 
Then Verify the Login page
And close the browser

Examples:
|uname||pass|
|1223233@apptium.com||Pass@1223|